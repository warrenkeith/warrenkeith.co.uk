module.exports = function (grunt) {
    grunt
    require('load-grunt-tasks')(grunt);

    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),

        // This is a list of the folders where stuff goes and is taken from
        dirs: {
            js: 'scripts/',
            scss: 'scss/',
            css: 'css/',
            img: 'img/',
            partials: 'partials/'
        },

        sass: {
            options: {
                sourcemap: 'inline',
                lineNumbers: true,
                debugInfo: true
            },
            app: {
                files: {
                    '<%= dirs.css %>/style.css': '<%= dirs.scss %>/style.scss'
                }
            }
        },

        watch: {
            options: {
                spawn: true
            },
            scss: {
                files: ['<%= dirs.scss %>**'],
                tasks: ['sass:app']
            },
            bower: {
                files: ['bower.json'],
                tasks: ['wiredep']
            }
        },

        wiredep: {
          task: {
              src: ['index.html']
          }
        },

        browserSync: {
            dev: {
                options: {
                    files: ['<%= dirs.css %>', '<%= dirs.js %>', 'index.html', 'partials/*'],
                    watchTask: true,
                    server: {
                        baseDir: ""
                    }
                }
            }
        }

    });

    grunt.registerTask('default', ['sass:app', 'wiredep', 'browserSync', 'watch']);

};