// Thoughts

app.factory('postsFactory', function ($http) {

    var response = $http.get('http://warrenkeith.co.uk/api/wp-json/wp/v2/posts');

    var factory = {};

    factory.list = function () {
        return response;
    };

    factory.get = function (id) {
       var getTheChosen = factory.list().then(function (res) {
            var chosen =  _.find(res.data, {'id': id});
            return chosen;
        });
        return getTheChosen;
    };

    return factory;

});

app.controller('ThoughtsController', function ($scope, postsFactory) {
    postsFactory.list()
        .then(function (data) {
            $scope.posts = data;
        });
});

app.controller('PostDetailController', function ($scope, postsFactory, $routeParams) {
    postsFactory.get(parseInt($routeParams.postId)).then(function (data) {
        $scope.postDetail = data;
    });
});