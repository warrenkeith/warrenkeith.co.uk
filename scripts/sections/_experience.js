// Experience

app.controller('ExperienceController', function ($scope, websitePopup) {

    // click the c64 button to see the balloon float by
    $scope.c64 = _.throttle(function () {
        var target = $('body');
        target.addClass('overflowHidden').append('<img class="c64-background" src="../../images/experience/c64-background.gif"><img class="c64-balloon" src="../../images/experience/c64-balloon.png">');
        var b = $('.c64-balloon, .c64-background');
        setTimeout(function () {
            b.remove();
            target.removeClass('overflowHidden');
        }, 3000);
    }, 3000, {'trailing': false});

});