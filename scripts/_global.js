app.service('websitePopup', function () {
    this.test = function (str) {
        console.log(str);
    };
});

app.controller('globalController', function ($scope, $route) {
    // bring through the current $route object from the config for nav highlighting
    $scope.route = $route;

    var tracking = function () {
        console.log('tracking...');
        $(document).on('click', '[data-track]', function () {
            var tracking = $(this).data('track');
            var href = $(this).attr('href');
            var event = '';
            if (tracking === '') {
                event = href;
            } else {
                event = tracking;
            }
            ga('send', {
                hitType: 'event',
                eventCategory: event,
                eventAction: 'click'
            });
        });

        $(document).on('click', '.wysiwyg a', function () {
            var href = $(this).attr('href');
            ga('send', {
                hitType: 'event',
                eventCategory: href,
                eventAction: 'click'
            });
        })
    }

    if (!document.location.hostname == "localhost") {
        tracking();
    }


});