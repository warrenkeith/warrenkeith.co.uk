// ngRoute Config

app.config(function($routeProvider){
   $routeProvider
       .when('/', {
           templateUrl: 'partials/home.html',
           controller: 'HomeController',
           slug: 'home'
       })
       .when('/thoughts', {
           templateUrl: 'partials/thoughts.html',
           controller: 'ThoughtsController',
           slug: 'thoughts'
        })
       .when('/thoughts/:postId', {
            controller: 'PostDetailController',
           templateUrl: 'partials/postDetails.html',
           slug: 'thoughts'
       })
       .when('/experience', {
           templateUrl: 'partials/experience.html',
           controller: 'ExperienceController',
           slug: 'experience'
       })
       .when('/skillset', {
           templateUrl: 'partials/skillset.html',
           slug: 'skillset'
       })
       .otherwise({
           template: '<div>Hey! Don\'t randomly try URLs! I didn\'t try to break your site, don\'t to break mine! The nerve.</div>'
       })
});